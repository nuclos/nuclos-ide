//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.Serializable;

import org.springframework.remoting.support.RemoteInvocationResult;

public class NuclosRemoteInvocationResult extends RemoteInvocationResult {
	
	Serializable userObject;


	public NuclosRemoteInvocationResult(Object value) {
		super(value);
	}

	public NuclosRemoteInvocationResult(Throwable exception) {
		super(exception);
	}
	
	public void setUserObject(Serializable o) {
		this.userObject = o;
	}
	
	public Serializable getUserObject() {
		return this.userObject;
	}

}
