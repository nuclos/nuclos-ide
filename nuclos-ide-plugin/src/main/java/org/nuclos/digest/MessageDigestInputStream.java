//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.digest;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.eclipse.equinox.security.storage.EncodingUtils;
import org.nuclos.ide.NuclosFatalIdeException;

/**
 * Calculate a crypto hash while reading an InputStream.
 *  
 * @author Thomas Pasch
 */
public class MessageDigestInputStream extends FilterInputStream {

	private static final int BUFFER_SIZE = 1024 * 16;

	//

	private final MessageDigest digest;

	public MessageDigestInputStream(InputStream in, String algo) throws NuclosFatalIdeException {
		super(in);
		try {
			digest = MessageDigest.getInstance(algo);
		} catch (NoSuchAlgorithmException e) {
			throw new NuclosFatalIdeException(e);
		}
	}

	@Override
	public int read() throws IOException {
		final int result = super.read();
		if (result >= 0) {
			digest.update((byte) result);
		}
		return result;
	}

	@Override
	public int read(byte b[]) throws IOException {
		final int result = super.read(b);
		if (result > 0) {
			digest.update(b, 0, result);
		}
		return result;
	}

	@Override
	public int read(byte b[], int off, int len) throws IOException {
		final int result = super.read(b, off, len);
		if (result > 0) {
			digest.update(b, off, result);
		}
		return result;
	}

	@Override
	public long skip(long n) throws IOException {
		final byte[] buffer = new byte[BUFFER_SIZE];
		long real = 0;
		int len = 0;
		while (n > 0) {
			if (n >= BUFFER_SIZE && len >= 0) {
				len = read(buffer);
			} else {
				len = read(buffer, 0, (int) n);
			}
			n -= len;
			real += len;
		}
		return real;
	}

	@Override
	public void mark(int readlimit) {
		throw new IllegalStateException("mark not supported");
	}

	@Override
	public boolean markSupported() {
		return false;
	}

	@Override
	public void reset() throws IOException {
		throw new IOException("mark not supported");
	}

	public byte[] digest() {
		return digest.digest();
	}

	public String digestAsHex() {
		return new String(org.nuclos.digest.MessageDigest.bytesToHex(digest.digest()));
	}
	
	public String digestAsBase64() {
		return EncodingUtils.encodeBase64(digest.digest());
	}

}
