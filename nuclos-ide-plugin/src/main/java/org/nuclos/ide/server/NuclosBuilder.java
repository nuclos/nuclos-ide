//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.server;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.nuclos.ide.listener.FileChangeDeltaVisitor;

/**
 * Builder associated with Nuclos server nature. Use to track if rules/event 
 * object sources are changed within eclipse.
 * 
 * @author Thomas Pasch
 */
public class NuclosBuilder extends IncrementalProjectBuilder {
	
	public static final String BUILDER_ID = "org.nuclos.ide.builder.server";
	
	private static final Map<IProject,IProject> projectsJustDownloaded = new ConcurrentHashMap<IProject, IProject>();
	
	//
	
	private IProject currentProject;
	
	public NuclosBuilder() {
	}
	
	public static void justDownloaded(IProject project) {
		projectsJustDownloaded.put(project, project);
	}

	@Override
	protected IProject[] build(int kind, Map<String, String> params, IProgressMonitor monitor) throws CoreException {
		currentProject = getProject();
		if (projectsJustDownloaded.remove(currentProject) != null) {
			return null;
		}
		
		switch (kind) {
		case IncrementalProjectBuilder.AUTO_BUILD:
		case IncrementalProjectBuilder.INCREMENTAL_BUILD:
			getDelta(currentProject).accept(new FileChangeDeltaVisitor(currentProject));
			break;
		}
		
		return null;
	}

}
