//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.server;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;
import org.nuclos.ide.Utils;

/**
 * Nuclos server nature.
 * 
 * @author Maik Stuecker
 */
public class ServerNature implements IProjectNature {

	public static final String NATURE_ID = "org.nuclos.ide.nature.server";
	
	public static final QualifiedName PROPERTY_HOST = new QualifiedName(NATURE_ID, "host");
	public static final QualifiedName PROPERTY_PORT = new QualifiedName(NATURE_ID, "port");
	public static final QualifiedName PROPERTY_INSTANCE = new QualifiedName(NATURE_ID, "instance");
	public static final QualifiedName PROPERTY_DEBUG_PORT = new QualifiedName(NATURE_ID, "debugPort");
	public static final QualifiedName PROPERTY_NUCLOS_VERSION = new QualifiedName(NATURE_ID, "nuclosVersion");
	public static final QualifiedName PROPERTY_LAUNCHLISTENER_NAME = new QualifiedName(NATURE_ID, "launchListenerName");
	
	public static final String DEFAULT_CHARSET = "UTF-8";
	public static final String DIGEST = "SHA-512";
	
	public static boolean FORCE_FILE = false;
	public static boolean FORCE_FOLDER = false;
	
	public static final String SERVERRULE = "serverrule";
	public static final String BUSINESSOBJECT = "businessobject";
	public static final String STATEMODEL = "statemodel";
	public static final String USERROLE = "role";
	public static final String OBJECT_GENERATION = "generation";
	public static final String REPORT = "report";
	public static final String REPORT_DATASOURCE = "reportDatasource";
	public static final String IMPORTSTRUCTURE_DEFINITION = "importStructure";
	public static final String PRINTOUT = "printout";
	public static final String COMMUNICATION = "communication";
	public static final String PARAMETER = "parameter";
	
	public static final String JAVA_LIBS = "libs";
	
	//
	
	private IProject project;
	
	public ServerNature() {
	}
	
	@Override
	public void configure() throws CoreException {
		Utils.ensureBuilder(project);
	}

	@Override
	public void deconfigure() throws CoreException {
		Utils.removeBuilder(project);
	}

	@Override
	public IProject getProject() {
		return project;
	}

	@Override
	public void setProject(IProject project) {
		this.project = project;
	}

}
