//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.server;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.nuclos.ide.Utils;
import org.nuclos.ide.singleton.Errors;

/**
 * Handler/listener to add the Nuclos server nature to an java project.
 * 
 * @author Thomas Pasch
 */
public class AddNature extends AbstractHandler implements MouseListener {

	private IJavaProject javaProject;
	
	public AddNature() {
	}

	public AddNature(IJavaProject project) {
		this.javaProject = project;
	}
	
	private IJavaProject getJavaProject() {
		if (javaProject == null) {
			javaProject = Utils.getSelectedProject();
		}
		return javaProject;
	}

	@Override
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		execute();
		return null;
	}

	private void execute() {
		final IProject myProj = getJavaProject().getProject();
		final Errors errors = Errors.getInstance(myProj);
		errors.clear();
		
		try {
			final IProjectDescription desc = myProj.getDescription();
			final String[] prevNatures = desc.getNatureIds();
			final String[] newNatures = new String[prevNatures.length + 1];
			System.arraycopy(prevNatures, 0, newNatures, 0, prevNatures.length);
			newNatures[prevNatures.length] = ServerNature.NATURE_ID;
			desc.setNatureIds(newNatures);
			myProj.setDescription(desc, null);
		} catch (CoreException e) {
			errors.addError("Adding Nuclos Server nature failed: " + e);
		}		
		
		errors.showOnErrors();
	}

	@Override
	public void mouseDoubleClick(MouseEvent arg0) {
	}

	@Override
	public void mouseDown(MouseEvent arg0) {
	}

	@Override
	public void mouseUp(MouseEvent arg0) {
		execute();
	}

}
