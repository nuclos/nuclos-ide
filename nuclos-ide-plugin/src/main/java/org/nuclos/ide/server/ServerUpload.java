//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.server;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.nuclos.ide.Activator;
import org.nuclos.ide.Utils;
import org.nuclos.ide.singleton.Errors;

/**
 * Handler/listener triggering uploading rules/event object source to Nuclos server.
 * 
 * @author Maik Stuecker, Thomas Pasch
 */
public class ServerUpload extends AbstractHandler implements MouseListener {
	
	private IJavaProject javaProject;
	
	public ServerUpload() {
	}

	public ServerUpload(IJavaProject project) {
		this.javaProject = project;
	}
	
	private IJavaProject getJavaProject() {
		if (javaProject == null) {
			javaProject = Utils.getSelectedProject();
		}
		return javaProject;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		execute();
		return null;
	}
	
	private void execute() {
		final IProject project = getJavaProject().getProject();
		final Errors errors = Errors.getInstance(project);
		errors.clear();
		final ServerTransfer transfer = new ServerTransfer(getJavaProject());
		final IWorkspace ws = getJavaProject().getProject().getWorkspace();
		
		final WorkspaceJob job = new WorkspaceJob("Nuclos Server Upload") {
			
			@Override
			public IStatus runInWorkspace(IProgressMonitor monitor) throws CoreException {
				try {
					// wait for auto-build to complete
					// http://www.eclipse.org/articles/Article-Builders/builders.html
					if (ws.isAutoBuilding()) {
						Job.getJobManager().join(ResourcesPlugin.FAMILY_AUTO_BUILD, null);
					}
					transfer.upload(monitor);
					return errors.getStatus();
				} catch (InterruptedException e) {
					final Status result = new Status(IStatus.CANCEL, Activator.PLUGIN_ID, e.toString());
					return result;
				} finally {
					errors.showOnErrors();
				}
			}
		};
		// we need exclusive access to project resources (Files, Folders)
		job.setRule(getJavaProject().getProject());
		job.setUser(true);
		job.setPriority(Job.LONG);
		job.schedule();
	}

	@Override
	public void mouseDoubleClick(MouseEvent arg0) {
	}

	@Override
	public void mouseDown(MouseEvent arg0) {
	}

	@Override
	public void mouseUp(MouseEvent arg0) {
		execute();
	}

}
