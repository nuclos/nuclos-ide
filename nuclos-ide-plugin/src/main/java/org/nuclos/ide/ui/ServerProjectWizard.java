//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.ui;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.internal.resources.ResourceException;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.internal.ui.util.ExceptionHandler;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jdt.launching.LibraryLocation;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.nuclos.ide.NuclosFatalIdeException;
import org.nuclos.ide.server.ServerDebug;
import org.nuclos.ide.server.ServerDownload;
import org.nuclos.ide.server.ServerNature;
import org.nuclos.ide.singleton.Errors;
import org.nuclos.ide.singleton.HttpSingleton;

/**
 * Links:
 * 
 * Custom Eclipse Wizard
 * http://www.ibm.com/developerworks/opensource/library/os-eclipse-custwiz/index.html
 * 
 * Java Project without Wizard (featured)
 * http://www.pushing-pixels.org/2008/11/18/extending-eclipse-creating-a-java-project-without-displaying-a-wizard.html
 * 
 * Java Based Project
 * http://stackoverflow.com/questions/6660155/eclipse-plugin-java-based-project-how-to
 * 
 * NewJavaProjectWizardPageTwo Javadoc
 * http://help.eclipse.org/indigo/topic/org.eclipse.jdt.doc.isv/reference/api/org/eclipse/jdt/ui/wizards/NewJavaProjectWizardPageTwo.html
 */
public class ServerProjectWizard extends Wizard implements INewWizard {
	
	private static final IClasspathEntry[] EMPTY_ICLASSPATHENTRY = new IClasspathEntry[0];

	protected Page1 one;
	protected Page2 two;

	public ServerProjectWizard() {
		super();
		setNeedsProgressMonitor(true);
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		
	}

	@Override
	public void addPages() {
		two = new Page2();
		two.setTitle("Nuclos server project");
		two.setDescription("Create a new Nuclos server project");
		two.setInitialProjectName("nuclos-rules");
		
		one = new Page1(two.getProjectHandle());
		addPage(one);
		addPage(two);
	}

	@Override
	public boolean performFinish() {
		final IProject project = two.getProjectHandle();
		final Errors errors = Errors.getInstance(project);
		errors.clear();
		try {	
			//create project
			URI location = null;
			if (!two.useDefaults()) {
				location = two.getLocationURI();
			}
			final IWorkspace workspace = ResourcesPlugin.getWorkspace();
			
			final IProjectDescription description = workspace.newProjectDescription(project.getName());
			description.setLocationURI(location);
			try {
				project.create(description, null);
			} catch (ResourceException e) {
				if (!project.exists()) {
					throw new NuclosFatalIdeException(e);
				}
			}
			project.open(null);
			project.setDefaultCharset(ServerNature.DEFAULT_CHARSET, null);
			
			//add natures
			description.setNatureIds(new String[] { JavaCore.NATURE_ID, ServerNature.NATURE_ID });
			project.setDescription(description, null);
			final IJavaProject javaProject = JavaCore.create(project); 
			final IProject myProject = javaProject.getProject();
			
			//create bin folder
			final IFolder binFolder = project.getFolder("bin");
			if (!binFolder.exists()) {
				binFolder.create(ServerNature.FORCE_FOLDER, true, null);
			}
			javaProject.setOutputLocation(binFolder.getFullPath(), null);
			
			//create libs folder
			final IFolder libsFolder = project.getFolder(ServerNature.JAVA_LIBS);
			if (!libsFolder.exists()) {
				libsFolder.create(ServerNature.FORCE_FOLDER, true, null);
			}

			//create src folder
			final IFolder serverrule = project.getFolder(ServerNature.SERVERRULE);
			if (!serverrule.exists()) {
				serverrule.create(ServerNature.FORCE_FOLDER, true, null);
			}
			final IFolder businessobject = project.getFolder(ServerNature.BUSINESSOBJECT);
			if (!businessobject.exists()) {
				businessobject.create(ServerNature.FORCE_FOLDER, true, null);
			}
			final IFolder statemodel = project.getFolder(ServerNature.STATEMODEL);
			if (!statemodel.exists()) {
				statemodel.create(ServerNature.FORCE_FOLDER, true, null);
			}
			final IFolder userrole = project.getFolder(ServerNature.USERROLE);
			if (!userrole.exists()) {
				userrole.create(ServerNature.FORCE_FOLDER, true, null);
			}
			final IFolder generation = project.getFolder(ServerNature.OBJECT_GENERATION);
			if (!generation.exists()) {
				generation.create(ServerNature.FORCE_FOLDER, true, null);
			}
			final IFolder report = project.getFolder(ServerNature.REPORT);
			if (!report.exists()) {
				report.create(ServerNature.FORCE_FOLDER, true, null);
			}
			final IFolder reportDs = project.getFolder(ServerNature.REPORT_DATASOURCE);
			if (!reportDs.exists()) {
				reportDs.create(ServerNature.FORCE_FOLDER, true, null);
			}
			final IFolder isDef = project.getFolder(ServerNature.IMPORTSTRUCTURE_DEFINITION);
			if (!isDef.exists()) {
				isDef.create(ServerNature.FORCE_FOLDER, true, null);
			}
			final IFolder printout = project.getFolder(ServerNature.PRINTOUT);
			if (!printout.exists()) {
				printout.create(ServerNature.FORCE_FOLDER, true, null);
			}
			final IFolder communication = project.getFolder(ServerNature.COMMUNICATION);
			if (!communication.exists()) {
				communication.create(ServerNature.FORCE_FOLDER, true, null);
			}
			final IFolder parameter = project.getFolder(ServerNature.PARAMETER);
			if (!parameter.exists()) {
				parameter.create(ServerNature.FORCE_FOLDER, true, null);
			}
			
			//add libs to project class path
			final List<IClasspathEntry> root = new ArrayList<IClasspathEntry>();
			javaProject.setRawClasspath(root.toArray(EMPTY_ICLASSPATHENTRY), null);
			
			//add src to project class path
			final IPackageFragmentRoot pfroot1 = javaProject.getPackageFragmentRoot(serverrule);
			final IPackageFragmentRoot pfroot2 = javaProject.getPackageFragmentRoot(businessobject);
			final IPackageFragmentRoot pfroot3 = javaProject.getPackageFragmentRoot(statemodel);
			final IPackageFragmentRoot pfroot4 = javaProject.getPackageFragmentRoot(generation);
			final IPackageFragmentRoot pfroot5 = javaProject.getPackageFragmentRoot(report);
			final IPackageFragmentRoot pfroot6 = javaProject.getPackageFragmentRoot(reportDs);
			final IPackageFragmentRoot pfroot7 = javaProject.getPackageFragmentRoot(isDef);
			final IPackageFragmentRoot pfroot8 = javaProject.getPackageFragmentRoot(printout);
			final IPackageFragmentRoot pfroot9 = javaProject.getPackageFragmentRoot(parameter);
			final IPackageFragmentRoot pfroot10 = javaProject.getPackageFragmentRoot(userrole);
			final IPackageFragmentRoot pfroot11 = javaProject.getPackageFragmentRoot(communication);
			
			final IClasspathEntry[] oldEntries = javaProject.getRawClasspath();
			final int oldLenght = oldEntries.length;
			final List<IClasspathEntry> newEntries = new ArrayList<IClasspathEntry>(oldLenght);
			for (IClasspathEntry cpe: oldEntries) {
				newEntries.add(cpe);
			}
			newEntries.add(JavaCore.newSourceEntry(pfroot1.getPath()));
			newEntries.add(JavaCore.newSourceEntry(pfroot2.getPath()));
			newEntries.add(JavaCore.newSourceEntry(pfroot3.getPath()));
			newEntries.add(JavaCore.newSourceEntry(pfroot4.getPath()));
			newEntries.add(JavaCore.newSourceEntry(pfroot5.getPath()));
			newEntries.add(JavaCore.newSourceEntry(pfroot6.getPath()));
			newEntries.add(JavaCore.newSourceEntry(pfroot7.getPath()));
			newEntries.add(JavaCore.newSourceEntry(pfroot8.getPath()));
			newEntries.add(JavaCore.newSourceEntry(pfroot11.getPath()));
			newEntries.add(JavaCore.newSourceEntry(pfroot10.getPath()));
			newEntries.add(JavaCore.newSourceEntry(pfroot9.getPath()));
			
			//get system libs
			final IVMInstall vmInstall = JavaRuntime.getDefaultVMInstall();
			final LibraryLocation[] locations = JavaRuntime.getLibraryLocations(vmInstall);
			for (LibraryLocation element : locations) {
				newEntries.add(JavaCore.newLibraryEntry(element.getSystemLibraryPath(), null, null));
			}

			javaProject.setRawClasspath(newEntries.toArray(EMPTY_ICLASSPATHENTRY), null);
			
			//write properties to project
			myProject.setPersistentProperty(ServerNature.PROPERTY_HOST, one.getHost());
			myProject.setPersistentProperty(ServerNature.PROPERTY_PORT, one.getPort());
			myProject.setPersistentProperty(ServerNature.PROPERTY_INSTANCE, one.getInstance());
			myProject.setPersistentProperty(ServerNature.PROPERTY_DEBUG_PORT, one.getDebugPort());
			
			//create debug launch config
			ServerDebug.createConfiguration(javaProject.getProject());		
			
			//download sources
			ServerDownload.execute(javaProject);
		} catch (CoreException e) {
			// throw new NuclosFatalIdeException(e);
			ExceptionHandler.handle(e, "Nuclos Server Project Wizard failed", e.toString());
		}
		return true;
	}

	private class Page1 extends WizardPage implements KeyListener, MouseListener, SelectionListener {
		
		private final IProject project;

		private Text txtHost;
		private Text txtPort;
		private Text txtInstance;
		private Text txtDebugPort;
		
		private boolean connectionIsValid = false;
		
		private Button btnTest;
		private Label lblTestResult;
		
		private Composite container;

		protected Page1(IProject project) {
			super("Nuclos server configuration");
			this.project = project;
			if (project == null) {
				throw new NullPointerException();
			}
			
			setTitle("Nuclos server configuration");
			setDescription("Set the server connect parameters and test the connection.");
			setControl(txtHost);
		}

		public String getHost() {
			return txtHost.getText();
		}
		
		public String getPort() {
			return txtPort.getText();
		}
		
		public String getInstance() {
			return txtInstance.getText();
		}
		
		public String getDebugPort() {
			return txtDebugPort.getText();
		}
		
		@Override
		public void createControl(Composite parent) {
			container = new Composite(parent, SWT.NULL);
			GridLayout layout = new GridLayout();
			container.setLayout(layout);
			layout.numColumns = 2;
			GridData gd = new GridData(GridData.FILL_HORIZONTAL);
			
			new Label(container, SWT.NULL).setText("Host");

			txtHost = new Text(container, SWT.BORDER | SWT.SINGLE);
			txtHost.setText("localhost");
			txtHost.addKeyListener(this);
			txtHost.setLayoutData(gd);
			
			new Label(container, SWT.NULL).setText("Port");

			txtPort = new Text(container, SWT.BORDER | SWT.SINGLE);
			txtPort.setText("80");
			txtPort.addKeyListener(this);
			txtPort.setLayoutData(gd);
			
			new Label(container, SWT.NULL).setText("Instance");

			txtInstance = new Text(container, SWT.BORDER | SWT.SINGLE);
			txtInstance.setText("nuclos");
			txtInstance.addKeyListener(this);
			txtInstance.setLayoutData(gd);
			
			new Label(container, SWT.NULL).setText("Debug port");

			txtDebugPort = new Text(container, SWT.BORDER | SWT.SINGLE);
			txtDebugPort.setText("8000");
			txtDebugPort.addKeyListener(this);
			txtDebugPort.setLayoutData(gd);
			
			new Label(container, SWT.NULL).setText("");
			
			btnTest = new Button(container, SWT.PUSH);
			btnTest.setText("Test connection");
			btnTest.addMouseListener(this);
			btnTest.addSelectionListener(this);
			
			new Label(container, SWT.NULL).setText("");
			
			lblTestResult = new Label(container, SWT.NULL);
			lblTestResult.setLayoutData(gd);
			
			// Required to avoid an error in the system
			setControl(container);
			Dialog.applyDialogFont(getControl());
			
			setPageComplete(false);
		}

		@Override
		public void keyPressed(KeyEvent e) {
		}

		@Override
		public void keyReleased(KeyEvent e) {
			connectionIsValid = false;
			updateComplete();
		}

		@Override
		public void mouseDoubleClick(MouseEvent e) {
		}

		@Override
		public void mouseDown(MouseEvent e) {
		}

		@Override
		public void mouseUp(MouseEvent e) {
			testConnection();
		}
		
		@Override
		public void widgetSelected(SelectionEvent e) {
			testConnection();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
		}
		
		private void testConnection() {
			final HttpSingleton http = HttpSingleton.getInstance();
			String testResult = http.testConnection(project, getHost(), getPort(), getInstance());
			if (testResult == null) {
				testResult = "Successfully connected!";
				connectionIsValid = true;
			}
			lblTestResult.setText(testResult);
			lblTestResult.update();
			updateComplete();
		}
		
		private void updateComplete() {
			boolean complete = true;
			if (!connectionIsValid 
					|| txtHost.getText().isEmpty()
					|| txtPort.getText().isEmpty() 
					|| txtInstance.getText().isEmpty()
					|| txtDebugPort.getText().isEmpty()) {
				complete = false;
			}
			setPageComplete(complete);
		}
		
	}
	
	private class Page2 extends WizardNewProjectCreationPage {

		public Page2() {
			super("Nuclos server project");
		}
		
		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.ui.dialogs.WizardNewProjectCreationPage#createControl(org.eclipse.swt.widgets.Composite)
		 */
		public void createControl(Composite parent) {
			super.createControl(parent);
			Dialog.applyDialogFont(getControl());
		}
		
	}

}
