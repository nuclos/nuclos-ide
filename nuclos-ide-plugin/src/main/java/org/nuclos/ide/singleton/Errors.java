//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.singleton;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.widgets.Display;
import org.nuclos.ide.Activator;
import org.nuclos.ide.NuclosFatalIdeException;

/**
 * Central error handling singleton for Nuclos server eclipse plugin.
 * <p>
 * This singleton is used to collect all problems/errors within one user (gui)
 * transaction. All errors could then displayed in one alert dialog.
 * </p>
 * @author Thomas Pasch
 */
public class Errors {
	
	private static final ConcurrentHashMap<IProject,Errors> INSTANCES = new ConcurrentHashMap<IProject, Errors>();
	
	//
	
	private final List<String> msgList = new LinkedList<String>();
	
	private Errors() {
	}
	
	public static Errors getInstance(IProject project) {
		Errors result = INSTANCES.get(project);
		if (result == null) {
			result = new Errors();
			INSTANCES.put(project, result);
		}
		return result;
	}
	
	public void clear() {
		msgList.clear();
	}
	
	public void addError(String msg) {
		msgList.add(msg);
	}
	
	public void throwOnErrors() throws CoreException {
		if (!msgList.isEmpty()) {
			final String s = toString();
			System.out.println(s);
			throw new NuclosFatalIdeException(s);
		}
	}
	
	public void showOnErrors() {
		if (!msgList.isEmpty()) {
			final String s = toString();
			System.out.println(s);
			/*
			final Exception ex = new NuclosFatalIdeException("Nuclos IDE error");
			final IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, 1, s, ex);
			 */
			Display.getDefault().asyncExec(new Runnable() {
				
				@Override
				public void run() {
					ErrorDialog.openError(null, "Nuclos IDE error", s, getStatus());
				}
			});
		}
	}
	
	public IStatus getStatus() {
		final IStatus result;
		if (!msgList.isEmpty()) {
			result = new Status(IStatus.ERROR, Activator.PLUGIN_ID, toString());
		} else {
			result = Status.OK_STATUS;
		}
		return result;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		if (msgList.isEmpty()) {
			result.append("no errors");
		} else {
			result.append(msgList.size()).append(" error(s):\n");
			for (String m: msgList) {
				result.append(m).append("\n");
			}
		}
		return result.toString();
	}

}
