//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide;

import java.util.Comparator;

import org.eclipse.jdt.core.IClasspathEntry;

/**
 * An Comparator for {@link IClasspathEntry}s.
 * 
 * @author Thomas Pasch
 */
public class ClasspathEntryComparator implements Comparator<IClasspathEntry> {
	
	private static final ClasspathEntryComparator INSTANCE = new ClasspathEntryComparator();
	
	public static ClasspathEntryComparator getInstance() {
		return INSTANCE;
	}
	
	private ClasspathEntryComparator() {
	}

	@Override
	public int compare(IClasspathEntry o1, IClasspathEntry o2) {
		int result = 0;
		result = o1.getPath().toPortableString().compareTo(o2.getPath().toPortableString());
		return result;
	}

}
