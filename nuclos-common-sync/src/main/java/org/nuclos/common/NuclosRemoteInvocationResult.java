package org.nuclos.common;

import java.io.Serializable;

import org.springframework.remoting.support.RemoteInvocationResult;

public class NuclosRemoteInvocationResult extends RemoteInvocationResult {
	
	Serializable userObject;


	public NuclosRemoteInvocationResult(Object value) {
		super(value);
	}

	public NuclosRemoteInvocationResult(Throwable exception) {
		super(exception);
	}
	
	public void setUserObject(Serializable o) {
		this.userObject = o;
	}
	
	public Serializable getUserObject() {
		return this.userObject;
	}

}
